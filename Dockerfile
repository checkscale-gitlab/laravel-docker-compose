# Build the main image
FROM php:7.4.2-apache-buster as base

ENV DEBIAN_FRONTEND=noninteractive \
    APP_PATH=/var/www/html \
    NON_ROOT_USER=www-data \
    NON_ROOT_PUID=1000 \
    NON_ROOT_GUID=1000

WORKDIR $APP_PATH

RUN usermod -u $NON_ROOT_PUID $NON_ROOT_USER && \
    groupmod -g $NON_ROOT_GUID $NON_ROOT_USER && \
    sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf && \
    a2enmod rewrite

# Install dependencies
RUN apt-get update && apt-get install --no-install-recommends --no-install-suggests -y \
    libmagickwand-dev=8:6.9.10.23+dfsg-2.1 \
    libfreetype6-dev=2.9.1-3+deb10u1 \
    imagemagick=8:6.9.10.23+dfsg-2.1 \
    libcurl4-openssl-dev=7.64.0-4 \
    libxml2-dev=2.9.4+dfsg1-7+b3 \
    zlib1g-dev=1:1.2.11.dfsg-1 \
    libsqlite3-dev=3.27.2-3 \
    git=1:2.20.1-2+deb10u1 \
    libsodium-dev=1.0.17-1 \
    libjpeg-dev=1:1.5.2-2 \
    unzip=6.0-23+deb10u1 \
    libzip-dev=1.5.1-4 \
    python2=2.7.16-1 \
    zip=3.0-11+b1 && \
    rm -r /var/lib/apt/lists/* && \
    pecl install \
    imagick \
    xdebug

# PHP extensions
RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install \
    pdo_sqlite \
    pdo_mysql \
    opcache \
    mysqli \
    bcmath \
    pcntl \
    soap \
    intl \
    zip \
    gd && \
    docker-php-ext-enable \
    opcache.so \
    imagick \
    xdebug

# Install composer dependencies
FROM composer:1.9.3 as dependencies
WORKDIR /app
COPY composer.* ./
RUN composer install --no-dev --no-ansi --no-interaction --no-progress --no-scripts --no-suggest --no-autoloader

# Integrate the project and clean up
FROM base as main
COPY . .
COPY --from=dependencies /usr/bin/composer /usr/local/bin/composer
COPY --from=dependencies /app/vendor $APP_PATH/vendor

RUN cp .env.example .env && \
    composer dump-autoload && \
    php artisan route:clear && \
    php artisan config:clear && \
    php artisan view:clear && \
    chown -R www-data:www-data $APP_PATH

# Note: The following is inherited from the base image, no need to execute it:
# EXPOSE 80
# CMD ["apache2-foreground"]

